#!/usr/bin/env bash
# Original file from Girl Life ecv - Cat

set -x

# Set those lines to fit your setup.
# This is where glife.qsp will be copied. If you don't want to move it just comment (#) the line below.
#DESTDIR=../SOB

# The file that will be generated or open
QSPFILE=SOB_26_CE.qsp

#######################################################################

./txtmerge_CE.py NoCheats SOB_26_CE.txt
if [[ "$OSTYPE" == "linux-gnu" ]]; then
	./txt2gam.linux SOB_26_CE.txt "${QSPFILE}" 1> /dev/null
elif [[ "$OSTYPE" == "darwin"* ]]; then
	./txt2gam.mac SOB_26_CE.txt "${QSPFILE}" 1> /dev/null
elif [[ "$OSTYPE" == "msys" ]]; then
	if [[ "$MSYSTEM_CARCH" == "x86_64" ]]; then
		./txt2gam64.exe SOB_26_CE.txt "${QSPFILE}" 1> /dev/null
	else
		./txt2gam.exe SOB_26_CE.txt "${QSPFILE}" 1> /dev/null
	fi
fi
if [ -d "${DESTDIR}" ]; then
	cp --reflink=auto "${QSPFILE}" "${DESTDIR}"
fi

