# Community translation of SOB

## Most is machine translated, however all of the buttons in the game are currently all translated. The main body text will take awhile to fully complete.

## Character name changes
* Glad/Happy --- Rada
* Eugene ------- Eugenia
* Light -------- Sveta
* Kate --------- Katya
* Aurelius ----- Aurelia
* Baena -------- Bazhena
* Jan/Ian ------ Yana
* Twins -------- Daria & Maria

## Some important changes
Katya's father's questions have been fixed. Choices are now A, B, C or D and questions are clear.

## Scripts
These are more for devs to split the main qsp into separate files, easier to work
on with a more advanced editor. Plus merge the split files back into one large file.

## vicezealot mod files
Unzip into the unique_npc folder of the game directory.
## Bonus scenes 1 and 2
https://mega.nz/#!mq5hXYgS!BOgSFACaDkqz1oBR_pLh3BjBFa8VAyAdMERuKMMc8nM
